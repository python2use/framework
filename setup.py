
from setuptools import setup, find_packages

setup(
    name='python2use',
    version='0.0.3',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An example python package',
    long_description=open('README.txt').read(),
    install_requires=[
        'cffi',
        'python-decouple',
        'click',
        'tqdm',
        'pytz', #==2018.9
        'humanize',
        'Pygments',

        'python-decouple',
        'bpython',
        'uritemplate',
        'pyyaml',

        'msgpack',
        'Twisted[tls,http2]',

        'requests',
        'beautifulsoup4',
        'lxml',
        'unidecode',
        'Pillow',
        'feedparser',

        'PyDispatcher==2.0.5',

        'pytz>=2018.5',
        'python-dateutil==2.6.1',
        'croniter>=0.3.24',
    ],
    extras_require={
        'model': [
            'jsonschema',
        ],
        'daten': [
            'pandas',
        ],
        'queue': [
            'rq',
            'rq-scheduler',
            'flower',
        ],
        'shell': [
            'paramiko',
        ],
    },
    url='https://github.com/python2use/framework',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com'
)
